# CIS 498 Group 3
# QR/Barcode Scanning
# Cole, Pete, Brian, Jimmy

import cv2
from pyzbar import pyzbar


def read_codes(frame):
    barcodes = pyzbar.decode(frame)
    for barcode in barcodes:
        x, y, w, h = barcode.rect

        barcode_info = barcode.data.decode('utf-8')
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)

        font = cv2.FONT_HERSHEY_PLAIN
        cv2.putText(frame, barcode_info, (x + 6, y - 6), font, 4.0, (0, 0, 0), 2)
    return frame

def main():

    camera = cv2.VideoCapture('newtest1.mp4')
    ret, frame = camera.read()

    while ret:
        ret, frame = camera.read()
        frame = read_codes(frame)
        cv2.imshow('QR Scanner', frame)
        if cv2.waitKey(1) & 0xFF == 27:
            break

    camera.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()